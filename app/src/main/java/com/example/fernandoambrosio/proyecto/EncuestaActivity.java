package com.example.fernandoambrosio.proyecto;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class EncuestaActivity extends AppCompatActivity {


    private EditText et1,et2,et4,et5,et7,et8,et9;
    private Button compartir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encuesta);

        et1= (EditText) findViewById(R.id.et1);
        et2= (EditText) findViewById(R.id.et2);

        et4= (EditText) findViewById(R.id.et4);
        et5= (EditText) findViewById(R.id.et5);
        et7= (EditText) findViewById(R.id.et7);
        et8= (EditText) findViewById(R.id.et8);
        et9= (EditText) findViewById(R.id.et9);



    }

    public void enviar(View view) {

        DbHelper admin = new DbHelper(this);
        SQLiteDatabase db=admin.getWritableDatabase();
        String codigoCurso = et1.getText().toString();
        String nombreDocente = et2.getText().toString();
        String preparacionDocente=et4.getText().toString();
        String conocimientoDocente=et5.getText().toString();
        String contenidoApropiado=et7.getText().toString();
        String aplicacionPractica=et8.getText().toString();
        int calificacionGeneral=Integer.parseInt(et9.getText().toString());



        ContentValues registro = new ContentValues();

        registro.put("codigoCurso", codigoCurso);
        registro.put("nombreDocente", nombreDocente);
        registro.put("preparacionDocente", preparacionDocente);
        registro.put("conocimientoDocente", conocimientoDocente);
        registro.put("contenidoApropiado", contenidoApropiado);
        registro.put("aplicacionPractica", aplicacionPractica);
        registro.put("calificacionGeneral", calificacionGeneral);
        db.insert("encuesta", null, registro);
        db.close();
        et1.setText("");
        et2.setText("");
        et4.setText("");
        et5.setText("");
        et7.setText("");
        et8.setText("");
        et9.setText("");

        Toast.makeText(this, "Encuesta Enviada",
                Toast.LENGTH_SHORT).show();
    }

    public void salir(View view){
        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);
    }

    public void compartwa (View view){
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, "Acabo de realizar la Evaluacion de Docentes, hazla tu tambien!!!");
        startActivity(intent);
    }

}
