package com.example.fernandoambrosio.proyecto;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Message;

import com.example.fernandoambrosio.proyecto.Catedratico;

import java.util.ArrayList;

/**
 * Created by Fernando Ambrosio on 26/07/2017.
 */
public class DbHelper extends SQLiteOpenHelper {

    private static final String DB_NAME="evaluacion.sqlite";
    private static final int DB_SCHEME_VERSION = 1;


    public DbHelper(Context context) {
        super(context, DB_NAME, null,DB_SCHEME_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table usuarios(codigo integer primary key,usuario text,contrasena text)");
        db.execSQL("create table catedraticos(codigo integer primary key,nombre text,curso text, semestre text, carrera text)");
        db.execSQL("create table encuesta(numeroEncuesta int primary key,codigoCurso int, nombreDocente text, preparacionDocente text, conocimientoDocente text, contenidoApropiado text, aplicacionPractica text, calificacionGeneral int )");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("create table usuarios(codigo integer primary key,usuario text,contrasena text)");
        db.execSQL("create table catedraticos(codigo integer primary key,nombre text, curso text, semestre text, carrera text)");
        db.execSQL("create table encuesta(numeroEncuesta int primary key,codigoCurso text, nombreDocente text, preparacionDocente text, conocimientoDocente text, contenidoApropiado text, aplicacionPractica text, calificacionGeneral int )");
    }

}
