

package com.example.fernandoambrosio.proyecto;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MosCatedraticoActivity extends AppCompatActivity {

    private ArrayList<Catedratico> catedraticos = new ArrayList<>();
    private ListView lista;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mos_catedratico);
        lista = (ListView) findViewById(R.id.lista);
        llenar_lv();

    }


    public void llenar_lv(){
        DbHelper dbHelper = new DbHelper(this);
        if (dbHelper != null){
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            Cursor  cursor = db.rawQuery("SELECT * FROM catedraticos",null);

            if(cursor.moveToFirst()){
                do{
                    catedraticos.add(new Catedratico(cursor.getInt(0),cursor.getString(1),cursor.getString(2),cursor.getString(3), cursor.getString(4)));
                }while(cursor.moveToNext());
            }
        }


        String[] arreglo = new String[catedraticos.size()];
        for (int i=0; i<arreglo.length; i++){
            arreglo[i]= (catedraticos.get(i).getNombre()+ "      "+ catedraticos.get(i).getCurso() + "       "+ catedraticos.get(i).getSemestre() + "      "+ catedraticos.get(i).getCarrera()  );

        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MosCatedraticoActivity.this, android.R.layout.simple_list_item_1,arreglo);
        lista.setAdapter(adapter);
    }

    public void regresarad (View view){
        Intent e = new Intent(this, AdministradorActivity.class);
        startActivity(e);
    }
}
