package com.example.fernandoambrosio.proyecto;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class ResultadosActivity extends AppCompatActivity {

    private ArrayList<Encuesta> encuestas = new ArrayList<>();
    private ListView lt2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultados);
        lt2 = (ListView) findViewById(R.id.lt2);
        llenar_encuesta();
    }

    public void llenar_encuesta(){
        DbHelper dbHelper = new DbHelper(this);
        if (dbHelper != null){
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("SELECT * FROM encuesta",null);

            if(cursor.moveToFirst()){
                do{
                    encuestas.add(new Encuesta(cursor.getString(1),cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getString(5),cursor.getString(6),cursor.getString(7)));
                }while(cursor.moveToNext());
            }
        }


        String[] arreglo = new String[encuestas.size()];
        for (int i=0; i<arreglo.length; i++){
            arreglo[i]= (encuestas.get(i).getCodigoCurso()+ "        "+ encuestas.get(i).getNombreDocente() + "         "+ encuestas.get(i).getCalificacionGeneral() );

        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(ResultadosActivity.this, android.R.layout.simple_list_item_1,arreglo);
        lt2.setAdapter(adapter);
    }

    public void regresare (View view){
        Intent e = new Intent(this, AdministradorActivity.class);
        startActivity(e);
    }
}
