package com.example.fernandoambrosio.proyecto;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class CatedraticoActivity extends AppCompatActivity {

    private EditText txt6, txt7, txt8,txt9, txt10;
    private Button bt5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catedratico);

        txt10 = (EditText) findViewById(R.id.txt10);
        txt6 = (EditText) findViewById(R.id.txt6);
        txt7= (EditText) findViewById(R.id.txt7);
        txt8= (EditText) findViewById(R.id.txt8);
        txt9= (EditText) findViewById(R.id.txt9);
        bt5= (Button) findViewById(R.id.bt5);
    }


    public void registrarcat(View view){

        DbHelper admin=new DbHelper(this);
        SQLiteDatabase db=admin.getWritableDatabase();
        String codigo=txt10.getText().toString();
        String nombre=txt6.getText().toString();
        String curso=txt7.getText().toString();
        String semestre=txt8.getText().toString();
        String carrera=txt9.getText().toString();


        ContentValues values=new ContentValues();
        values.put("codigo",codigo);
        values.put("nombre",nombre);
        values.put("curso",curso);
        values.put("semestre",semestre);
        values.put("carrera",carrera);

        db.insert("catedraticos",null,values);
        db.close();

        Intent ven=new Intent(this,AdministradorActivity.class);
        startActivity(ven);

    }
}
