package com.example.fernandoambrosio.proyecto;

/**
 * Created by Fernando Ambrosio on 28/07/2017.
 */
public class Encuesta {
    String codigoCurso;
    String nombreDocente;
    String preparacionDocente;
    String conocimientoDocente;
    String contenidoApropiado;
    String aplicaciónPractica;
    String calificacionGeneral;

    public Encuesta() {
    }

    public Encuesta(String codigoCurso, String nombreDocente, String preparacionDocente, String conocimientoDocente, String contenidoApropiado, String aplicaciónPractica, String calificacionGeneral) {

        this.codigoCurso = codigoCurso;
        this.nombreDocente = nombreDocente;
        this.preparacionDocente = preparacionDocente;
        this.conocimientoDocente = conocimientoDocente;
        this.contenidoApropiado = contenidoApropiado;
        this.aplicaciónPractica = aplicaciónPractica;
        this.calificacionGeneral = calificacionGeneral;
    }


    public String getCodigoCurso() {
        return codigoCurso;
    }

    public void setCodigoCurso(String codigoCurso) {
        this.codigoCurso = codigoCurso;
    }

    public String getNombreDocente() {
        return nombreDocente;
    }

    public void setNombreDocente(String nombreDocente) {
        this.nombreDocente = nombreDocente;
    }

    public String getPreparacionDocente() {
        return preparacionDocente;
    }

    public void setPreparacionDocente(String preparacionDocente) {
        this.preparacionDocente = preparacionDocente;
    }

    public String getConocimientoDocente() {
        return conocimientoDocente;
    }

    public void setConocimientoDocente(String conocimientoDocente) {
        this.conocimientoDocente = conocimientoDocente;
    }

    public String getContenidoApropiado() {
        return contenidoApropiado;
    }

    public void setContenidoApropiado(String contenidoApropiado) {
        this.contenidoApropiado = contenidoApropiado;
    }

    public String getAplicaciónPractica() {
        return aplicaciónPractica;
    }

    public void setAplicaciónPractica(String aplicaciónPractica) {
        this.aplicaciónPractica = aplicaciónPractica;
    }

    public String getCalificacionGeneral() {
        return calificacionGeneral;
    }

    public void setCalificacionGeneral(String calificacionGeneral) {
        this.calificacionGeneral = calificacionGeneral;
    }
}