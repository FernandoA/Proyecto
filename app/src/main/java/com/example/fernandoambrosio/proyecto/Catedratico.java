package com.example.fernandoambrosio.proyecto;

/**
 * Created by Fernando Ambrosio on 28/07/2017.
 */
public class Catedratico {

    int codigo;
    String nombre, curso, semestre, carrera;

    public Catedratico() {
    }

    public Catedratico(int codigo, String nombre, String curso, String semestre, String carrera) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.curso = curso;
        this.semestre = semestre;
        this.carrera = carrera;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }
}
