package com.example.fernandoambrosio.proyecto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class AdministradorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_administrador);
    }

    public void registarcat(View view){
        Intent e = new Intent(this, CatedraticoActivity.class);
        startActivity(e);
    }

    public void mostrarcat(View view){
        Intent e = new Intent(this,MosCatedraticoActivity.class);
        startActivity(e);
    }

    public void mostrarresultado(View view){
        Intent e = new Intent(this, ResultadosActivity.class);
        startActivity(e);
    }

    public void regresarlo(View view){
        Intent e = new Intent(this,LoginActivity.class);
        startActivity(e);
    }
}
